﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScreenMon
{
    public partial class LogForm : Form
    {
        [DllImport("user32.dll")]
        static extern bool HideCaret(IntPtr hWnd);

        private MainForm parent;

        public LogForm(MainForm parent)
        {
            this.parent = parent;
            InitializeComponent();
            checkBoxLog.Checked = parent.keepLog;
            
            if (null != parent.logTimestamps)
            {
                foreach (var ts in parent.logTimestamps)
                {
                    txtBoxLog.AppendText(ts.ToString());
                    txtBoxLog.AppendText("\n");
                }
                txtBoxLog.ScrollToCaret();
            }
        }

        public void UpdateLog()
        {
            txtBoxLog.AppendText(parent.logTimestamps.Last.Value.ToString());
            txtBoxLog.AppendText("\n");
            txtBoxLog.ScrollToCaret();
        }

        private void checkBoxLog_CheckedChanged(object sender, EventArgs e)
        {
            parent.keepLog = checkBoxLog.Checked;
        }

        private void btnClearLog_Click(object sender, EventArgs e)
        {
            txtBoxLog.Clear();
            txtBoxLog.Refresh();
            if (null != parent.logTimestamps)
                parent.logTimestamps.Clear();
        }

        private void LogForm_Shown(object sender, EventArgs e)
        {
            this.ActiveControl = null;
        }
    }
}
