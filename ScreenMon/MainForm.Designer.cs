﻿namespace ScreenMon
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnStart = new System.Windows.Forms.Button();
            this.timerUpdate = new System.Windows.Forms.Timer(this.components);
            this.btnStop = new System.Windows.Forms.Button();
            this.btnSettings = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.contextSettingsMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.checkBoxPlaySound = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuSep = new System.Windows.Forms.ToolStripSeparator();
            this.menuItemSelectorLoc = new System.Windows.Forms.ToolStripMenuItem();
            this.topToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bottomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leftToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.labelInterval = new System.Windows.Forms.ToolStripTextBox();
            this.txtBoxInterval = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btnAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.btnOpenLog = new System.Windows.Forms.Button();
            this.contextSettingsMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.SlateGray;
            this.btnStart.BackgroundImage = global::ScreenMon.Properties.Resources.startMonitor;
            this.btnStart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStart.FlatAppearance.BorderSize = 0;
            this.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStart.Font = new System.Drawing.Font("Consolas", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Location = new System.Drawing.Point(9, 9);
            this.btnStart.Margin = new System.Windows.Forms.Padding(0);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(50, 50);
            this.btnStart.TabIndex = 0;
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.EnabledChanged += new System.EventHandler(this.btnStart_EnabledChanged);
            this.btnStart.Click += new System.EventHandler(this.StartMonitoring);
            // 
            // timerUpdate
            // 
            this.timerUpdate.Interval = 1000;
            this.timerUpdate.Tick += new System.EventHandler(this.Update);
            // 
            // btnStop
            // 
            this.btnStop.BackColor = System.Drawing.Color.SlateGray;
            this.btnStop.BackgroundImage = global::ScreenMon.Properties.Resources.stopMonitorDisabled;
            this.btnStop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStop.Enabled = false;
            this.btnStop.FlatAppearance.BorderSize = 0;
            this.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStop.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.Location = new System.Drawing.Point(59, 9);
            this.btnStop.Margin = new System.Windows.Forms.Padding(0);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(50, 50);
            this.btnStop.TabIndex = 3;
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.EnabledChanged += new System.EventHandler(this.btnStop_EnabledChanged);
            this.btnStop.Click += new System.EventHandler(this.StopMonitoring);
            // 
            // btnSettings
            // 
            this.btnSettings.BackColor = System.Drawing.Color.SlateGray;
            this.btnSettings.BackgroundImage = global::ScreenMon.Properties.Resources.settings;
            this.btnSettings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSettings.FlatAppearance.BorderSize = 0;
            this.btnSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSettings.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSettings.Location = new System.Drawing.Point(109, 9);
            this.btnSettings.Margin = new System.Windows.Forms.Padding(0);
            this.btnSettings.Name = "btnSettings";
            this.btnSettings.Size = new System.Drawing.Size(50, 50);
            this.btnSettings.TabIndex = 4;
            this.btnSettings.UseVisualStyleBackColor = false;
            this.btnSettings.EnabledChanged += new System.EventHandler(this.btnSettings_EnabledChanged);
            this.btnSettings.Click += new System.EventHandler(this.btnSettings_Click);
            this.btnSettings.MouseEnter += new System.EventHandler(this.btnSettings_MouseEnter);
            this.btnSettings.MouseLeave += new System.EventHandler(this.btnSettings_MouseLeave);
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.BackColor = System.Drawing.Color.SlateGray;
            this.btnExit.BackgroundImage = global::ScreenMon.Properties.Resources.exit;
            this.btnExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.Font = new System.Drawing.Font("Segoe UI", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.SystemColors.Window;
            this.btnExit.Location = new System.Drawing.Point(265, 0);
            this.btnExit.Margin = new System.Windows.Forms.Padding(0);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(35, 35);
            this.btnExit.TabIndex = 5;
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            this.btnExit.MouseEnter += new System.EventHandler(this.btnExit_MouseEnter);
            this.btnExit.MouseLeave += new System.EventHandler(this.btnExit_MouseLeave);
            // 
            // contextSettingsMenu
            // 
            this.contextSettingsMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextSettingsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkBoxPlaySound,
            this.toolStripMenuSep,
            this.menuItemSelectorLoc,
            this.toolStripSeparator1,
            this.labelInterval,
            this.txtBoxInterval,
            this.toolStripSeparator2,
            this.btnAbout});
            this.contextSettingsMenu.Name = "contextSettingsMenu";
            this.contextSettingsMenu.Size = new System.Drawing.Size(211, 151);
            this.contextSettingsMenu.Closing += new System.Windows.Forms.ToolStripDropDownClosingEventHandler(this.contextSettingsMenu_Closing);
            // 
            // checkBoxPlaySound
            // 
            this.checkBoxPlaySound.Checked = true;
            this.checkBoxPlaySound.CheckOnClick = true;
            this.checkBoxPlaySound.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxPlaySound.Name = "checkBoxPlaySound";
            this.checkBoxPlaySound.Size = new System.Drawing.Size(210, 26);
            this.checkBoxPlaySound.Text = "Play sound";
            this.checkBoxPlaySound.CheckedChanged += new System.EventHandler(this.checkBoxPlaySound_CheckedChanged);
            // 
            // toolStripMenuSep
            // 
            this.toolStripMenuSep.Name = "toolStripMenuSep";
            this.toolStripMenuSep.Size = new System.Drawing.Size(207, 6);
            // 
            // menuItemSelectorLoc
            // 
            this.menuItemSelectorLoc.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.topToolStripMenuItem,
            this.rightToolStripMenuItem,
            this.bottomToolStripMenuItem,
            this.leftToolStripMenuItem});
            this.menuItemSelectorLoc.Name = "menuItemSelectorLoc";
            this.menuItemSelectorLoc.Size = new System.Drawing.Size(210, 26);
            this.menuItemSelectorLoc.Text = "Selector location";
            // 
            // topToolStripMenuItem
            // 
            this.topToolStripMenuItem.Name = "topToolStripMenuItem";
            this.topToolStripMenuItem.Size = new System.Drawing.Size(134, 26);
            this.topToolStripMenuItem.Tag = "Top";
            this.topToolStripMenuItem.Text = "Top";
            this.topToolStripMenuItem.Click += new System.EventHandler(this.topToolStripMenuItem_Click);
            // 
            // rightToolStripMenuItem
            // 
            this.rightToolStripMenuItem.Name = "rightToolStripMenuItem";
            this.rightToolStripMenuItem.Size = new System.Drawing.Size(134, 26);
            this.rightToolStripMenuItem.Tag = "Right";
            this.rightToolStripMenuItem.Text = "Right";
            this.rightToolStripMenuItem.Click += new System.EventHandler(this.topToolStripMenuItem_Click);
            // 
            // bottomToolStripMenuItem
            // 
            this.bottomToolStripMenuItem.Name = "bottomToolStripMenuItem";
            this.bottomToolStripMenuItem.Size = new System.Drawing.Size(134, 26);
            this.bottomToolStripMenuItem.Tag = "Bottom";
            this.bottomToolStripMenuItem.Text = "Bottom";
            this.bottomToolStripMenuItem.Click += new System.EventHandler(this.topToolStripMenuItem_Click);
            // 
            // leftToolStripMenuItem
            // 
            this.leftToolStripMenuItem.Name = "leftToolStripMenuItem";
            this.leftToolStripMenuItem.Size = new System.Drawing.Size(134, 26);
            this.leftToolStripMenuItem.Tag = "Left";
            this.leftToolStripMenuItem.Text = "Left";
            this.leftToolStripMenuItem.Click += new System.EventHandler(this.topToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(207, 6);
            // 
            // labelInterval
            // 
            this.labelInterval.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelInterval.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.labelInterval.Enabled = false;
            this.labelInterval.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInterval.Name = "labelInterval";
            this.labelInterval.ReadOnly = true;
            this.labelInterval.Size = new System.Drawing.Size(150, 20);
            this.labelInterval.Text = "Update interval (ms)";
            // 
            // txtBoxInterval
            // 
            this.txtBoxInterval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBoxInterval.MaxLength = 10;
            this.txtBoxInterval.Name = "txtBoxInterval";
            this.txtBoxInterval.Size = new System.Drawing.Size(100, 27);
            this.txtBoxInterval.Text = "1000";
            this.txtBoxInterval.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBoxInterval_KeyPress);
            this.txtBoxInterval.TextChanged += new System.EventHandler(this.txtBoxInterval_TextChanged_1);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(207, 6);
            // 
            // btnAbout
            // 
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(210, 26);
            this.btnAbout.Text = "ScreenMon ";
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(190, 24);
            this.toolStripMenuItem1.Text = "Open log";
            // 
            // btnOpenLog
            // 
            this.btnOpenLog.BackColor = System.Drawing.Color.SlateGray;
            this.btnOpenLog.BackgroundImage = global::ScreenMon.Properties.Resources.log;
            this.btnOpenLog.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnOpenLog.FlatAppearance.BorderSize = 0;
            this.btnOpenLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOpenLog.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOpenLog.Location = new System.Drawing.Point(159, 9);
            this.btnOpenLog.Margin = new System.Windows.Forms.Padding(0);
            this.btnOpenLog.Name = "btnOpenLog";
            this.btnOpenLog.Size = new System.Drawing.Size(50, 50);
            this.btnOpenLog.TabIndex = 6;
            this.btnOpenLog.UseVisualStyleBackColor = false;
            this.btnOpenLog.Click += new System.EventHandler(this.btnOpenLog_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(300, 70);
            this.Controls.Add(this.btnOpenLog);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnSettings);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "ScreenMon";
            this.TopMost = true;
            this.TransparencyKey = System.Drawing.Color.Lime;
            this.AutoSizeChanged += new System.EventHandler(this.StopMonitoring);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.ClientSizeChanged += new System.EventHandler(this.StopMonitoring);
            this.LocationChanged += new System.EventHandler(this.StopMonitoring);
            this.SizeChanged += new System.EventHandler(this.StopMonitoring);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form_MouseUp);
            this.Move += new System.EventHandler(this.MoveSubForm);
            this.contextSettingsMenu.ResumeLayout(false);
            this.contextSettingsMenu.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Timer timerUpdate;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnSettings;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.ContextMenuStrip contextSettingsMenu;
        private System.Windows.Forms.ToolStripMenuItem menuItemSelectorLoc;
        private System.Windows.Forms.ToolStripMenuItem topToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rightToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bottomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leftToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox txtBoxInterval;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripTextBox labelInterval;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem checkBoxPlaySound;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuSep;
        private System.Windows.Forms.Button btnOpenLog;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem btnAbout;
    }
}

