﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Media;

namespace ScreenMon
{
    public partial class MainForm : Form
    {
        private Monitor monitor;
        private bool mouseDown;
        private Point lastLocation;
        private SelectionForm selForm;
        private LogForm logForm;
        private SoundPlayer soundPlayer;
        private Panel selectionPanel;
        private int updateInterval;
        private const int updateIntervalMin = 100;
        private bool playSound;
        private Color selPanelActive = Color.OrangeRed;
        private Color selPanelInactive = Color.SpringGreen;
        private bool settingsOpen = false;
        private bool settingsBtnHover = false;

        public bool keepLog;
        public LinkedList<DateTime> logTimestamps;

        public MainForm()
        {
            InitializeComponent();
            updateInterval = Properties.Settings.Default.UpdateInterval;
            playSound = Properties.Settings.Default.PlaySound;
            keepLog = Properties.Settings.Default.KeepLog;            
            soundPlayer = new System.Media.SoundPlayer(Properties.Resources.NotificationSound);
            
            this.MinimumSize = this.Size;
            selForm = new SelectionForm(this);
            selForm.Width = this.Width;
            selectionPanel = selForm.selectionPanel;
            
            selForm.Show();
            selForm.TopMost = true;
        }

        private void StartMonitoring(object sender, EventArgs e)
        {
            monitor = new Monitor(TakeScreenshot());
            btnStart.Enabled = false;
            btnStop.Enabled = true;
            btnSettings.Enabled = false;
            btnStop.Focus();

            timerUpdate.Interval = updateInterval;
            timerUpdate.Start();
                        
            selForm.BackColor = selPanelActive;
        }

        public void StopMonitoring(object sender, EventArgs e)
        {
            if (null == monitor) { return; }
            
            monitor = null;
            GC.Collect();

            timerUpdate.Stop();
            btnStart.Enabled = true;
            btnStop.Enabled = false;
            btnSettings.Enabled = true;
            btnStart.Focus();

            selForm.BackColor = selPanelInactive;
        }


        private void Update(object sender, EventArgs e)
        {
            // TODO: Add option to use locality sensitive hashing because Windows drop shadows
            // trigger a change when a window gets close to edge of the selector panel
            if (monitor.Diff(TakeScreenshot()))
            {
                if (playSound)
                    PlaySound();

                if (keepLog)
                {
                    if (null == logTimestamps)
                        logTimestamps = new LinkedList<DateTime>();
                    logTimestamps.AddLast(DateTime.Now);
                    
                    if (null != logForm && !logForm.IsDisposed)
                        logForm.UpdateLog();
                }
            }
        }

        private void PlaySound()
        {
            soundPlayer.Play();
        }

        private Bitmap TakeScreenshot()
        {
            Point scrPos = selForm.PointToScreen(selectionPanel.Location);
            using (var printscreen = new Bitmap(selectionPanel.Width, selectionPanel.Height, PixelFormat.Format32bppArgb))
            {
                using (var graphics = Graphics.FromImage(printscreen as Image))
                {
                    graphics.CopyFromScreen(scrPos.X, scrPos.Y, 0, 0, printscreen.Size);
                    
                    return printscreen.Clone() as Bitmap;
                }
            }
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            if (settingsOpen)
            {
                contextSettingsMenu.Close();
                settingsOpen = false;
            }
            else
            {
                txtBoxInterval.Text = updateInterval.ToString();
                var pos = this.PointToScreen(btnSettings.Location);
                pos.X += btnSettings.Width;
                pos.Y += btnSettings.Height;
                checkBoxPlaySound.Checked = playSound;
                contextSettingsMenu.Show(pos);

                settingsOpen = true;
            }
        }

        private void Form_MouseDown(object sender, MouseEventArgs e)
        {
            mouseDown = true;
            lastLocation = e.Location;
        }

        private void Form_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseDown)
            {
                this.Location = new Point(
                    (this.Location.X - lastLocation.X) + e.X, (this.Location.Y - lastLocation.Y) + e.Y);

                this.Update();
            }
        }

        private void Form_MouseUp(object sender, MouseEventArgs e)
        {
            mouseDown = false;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void MoveSubForm(object sender, EventArgs e)
        {
            if (selForm != null)
            {
                switch (selForm.parentAnchor)
                {
                    case AnchorStyles.Bottom:
                        selForm.Left = this.Left;
                        selForm.Top = this.Top - selForm.Height;
                        break;
                    case AnchorStyles.Left:
                        selForm.Left = this.Left + selForm.Width;
                        selForm.Top = this.Top;
                        break;
                    case AnchorStyles.Right:
                        selForm.Left = this.Left - selForm.Width;
                        selForm.Top = this.Top;
                        break;
                    case AnchorStyles.Top:
                        selForm.Left = this.Left;
                        selForm.Top = this.Bottom;
                        break;
                }
            }
        }

        private void topToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string tag = (sender as ToolStripMenuItem).Tag as string;
            switch (tag)
            {
                case "Top":
                    selForm.parentAnchor = AnchorStyles.Bottom;
                    break;
                case "Right":
                    selForm.parentAnchor = AnchorStyles.Left;
                    break;
                case "Bottom":
                    selForm.parentAnchor = AnchorStyles.Top;
                    break;
                case "Left":
                    selForm.parentAnchor = AnchorStyles.Right;
                    break;
            }
            MoveSubForm(null, null);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.SelectionFormAnchor = selForm.parentAnchor;
            Properties.Settings.Default.UpdateInterval = this.updateInterval;
            Properties.Settings.Default.PlaySound = this.playSound;
            Properties.Settings.Default.KeepLog = this.keepLog;
            Properties.Settings.Default.Save();
        }

        private void txtBoxInterval_TextChanged_1(object sender, EventArgs e)
        {
            txtBoxInterval.Text = Regex.Replace(txtBoxInterval.Text, "[^0-9]", "");
        }

        private void txtBoxInterval_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
            {
                contextSettingsMenu.Close();
                e.Handled = true;
                return;
            }

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
                return;
            }
        }

        private void contextSettingsMenu_Closing(object sender, ToolStripDropDownClosingEventArgs e)
        {
            int newVal;
            if (int.TryParse(txtBoxInterval.Text, out newVal))
            {
                if (newVal >= updateIntervalMin)
                    updateInterval = newVal;
                else
                    updateInterval = updateIntervalMin;
            }
            txtBoxInterval.Text = updateInterval.ToString();

            if (!settingsBtnHover)
                settingsOpen = false;
        }

        private void checkBoxPlaySound_CheckedChanged(object sender, EventArgs e)
        {
            playSound = checkBoxPlaySound.Checked;
        }

        private void btnOpenLog_Click(object sender, EventArgs e)
        {
            if (null == logForm || logForm.IsDisposed)
            {
                logForm = new LogForm(this);
                logForm.Show();
                if (this.Left - this.Width < 0)
                {
                    logForm.Left = selForm.Right > this.Right ? selForm.Right : this.Right;
                }
                else
                {
                    logForm.Left = this.Left - this.Width;
                }
                logForm.Top = this.Top;
            }
            else
            {
                logForm.Close();
            }
        }

        private void btnExit_MouseEnter(object sender, EventArgs e)
        {
            btnExit.BackColor = Color.Tomato;
        }

        private void btnExit_MouseLeave(object sender, EventArgs e)
        {
            btnExit.BackColor = Color.SlateGray;
        }

        private void btnStart_EnabledChanged(object sender, EventArgs e)
        {
            if (btnStart.Enabled)
                btnStart.BackgroundImage = Properties.Resources.startMonitor; 
            else
                btnStart.BackgroundImage = Properties.Resources.startMonitorDisabled;
        }

        private void btnStop_EnabledChanged(object sender, EventArgs e)
        {
            if (btnStop.Enabled)
                btnStop.BackgroundImage = Properties.Resources.stopMonitor;
            else
                btnStop.BackgroundImage = Properties.Resources.stopMonitorDisabled;
        }

        private void btnSettings_EnabledChanged(object sender, EventArgs e)
        {
            if (btnSettings.Enabled)
                btnSettings.BackgroundImage = Properties.Resources.settings;
            else
                btnSettings.BackgroundImage = Properties.Resources.settingsDisabled;
        }

        private void btnSettings_MouseEnter(object sender, EventArgs e)
        {
            settingsBtnHover = true;
        }

        private void btnSettings_MouseLeave(object sender, EventArgs e)
        {
            settingsBtnHover = false;
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://gitlab.com/kneels/ScreenMon");
        }
    }

}
