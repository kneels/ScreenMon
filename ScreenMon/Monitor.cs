﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Drawing.Imaging;

namespace ScreenMon
{
    public class Monitor
    {
        public Bitmap baseline;

        public Monitor(Bitmap firstSample)
        {
            baseline = firstSample.Clone() as Bitmap;
        }

        public bool Diff(Bitmap newScreenshot)
        {
            if (!baseline.CompareMemCmp(newScreenshot))
            {
                baseline.Dispose();
                baseline = newScreenshot;
                return true;
            }
            newScreenshot.Dispose();
            return false;
        }
    }
}
