﻿namespace ScreenMon
{
    partial class SelectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.selectionPanel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // selectionPanel
            // 
            this.selectionPanel.BackColor = System.Drawing.Color.Lime;
            this.selectionPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.selectionPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selectionPanel.Location = new System.Drawing.Point(10, 10);
            this.selectionPanel.Margin = new System.Windows.Forms.Padding(0);
            this.selectionPanel.Name = "selectionPanel";
            this.selectionPanel.Size = new System.Drawing.Size(236, 128);
            this.selectionPanel.TabIndex = 2;
            // 
            // SelectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SpringGreen;
            this.ClientSize = new System.Drawing.Size(256, 148);
            this.Controls.Add(this.selectionPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MinimumSize = new System.Drawing.Size(25, 25);
            this.Name = "SelectionForm";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.ShowInTaskbar = false;
            this.Text = "SelectionForm";
            this.TransparencyKey = System.Drawing.Color.Lime;
            this.SizeChanged += new System.EventHandler(this.SelectionForm_SizeChanged);
            this.ResumeLayout(false);
            this.TopMost = true;

        }

        #endregion

        public System.Windows.Forms.Panel selectionPanel;


    }
}